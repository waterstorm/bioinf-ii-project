package bioinfProjectPocketAminoAcid;
import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CountAminoAcidNumber {
	static public void main(String[] args) throws Exception{
		if(args.length!=1){
			throw new Exception("Usage: Please enter one filepath!");
		}
		
		//get all the files unter the file path
		String filePath=args[0];
		File f=new File(filePath);
		File[] files=f.listFiles();
		BufferedReader pdbFile=null;
		String line=null;
		int aaNumTotal=0;
		
		String aminoAcids[]={"ALA","ARG","ASN","ASP","CYS","GLN","GLU","GLY","HIS","ILE","LEU","LYS","MET","MSE","PHE","PRO","SER","THR","TRP","TYR","VAL"};
		int aminoAcidNum[]=new int[21];
		
		
		for(int i=0; i< files.length; i++){
			if(files[i].isFile()){
				
				//check the file type
				String file=files[i].toString();
				if(file.endsWith(".pdb")){
					pdbFile=new BufferedReader(new FileReader(file));
					    
						while((line=pdbFile.readLine())!=null){
				        	
				        	//calculate the total number of AAs
				        	if(line.startsWith("SEQRES")){
				        		for(int j=19; j<68; j+=4){
				        			String aa=line.substring(j,j+3);
				        			for(int m=0; m<21; m++){
				        				if(aa.equals(aminoAcids[m])){
				        					aaNumTotal++;
				        					aminoAcidNum[m]++;
				        				}
				        			}
				        		}
				        	}
				        }
				}
			}
		}
	
		DecimalFormat myformat=null;
		myformat=(DecimalFormat)NumberFormat.getPercentInstance();
		myformat.applyPattern("00.00%");
		
		//order the amino acids according to their frequency 
		for(int i=0;i<20;i++){
			for(int j=0;j<20;j++){
				if(aminoAcidNum[j]<aminoAcidNum[j+1]){
					int t=aminoAcidNum[j];
					aminoAcidNum[j]=aminoAcidNum[j+1];
					aminoAcidNum[j+1]=t;
					String tt=aminoAcids[j];
					aminoAcids[j]=aminoAcids[j+1];
					aminoAcids[j+1]=tt;
				}
			}
		}
		
	
	    File file=new File("Residus_analysis.txt");
		FileWriter writer=new FileWriter(file);
		writer.write("\r\nThe total number of AAs is: "+aaNumTotal);
		
		//get the frequencies of the amino acids 
		writer.write("\r\n\r\nThe frequencies of the amino acids are:");
		for(int i=0; i<21; i++){
			double percentageAA=(double)aminoAcidNum[i]/aaNumTotal;
			writer.write("\r\nThe number of ' "+aminoAcids[i]+" ' is: "+aminoAcidNum[i]+"  ("+myformat.format(percentageAA)+")");
		}
		
		writer.write("\r\n\r\n");
		
		writer.flush();
		writer.close();
		
		System.out.println("Please see the results in the file 'Results for pdbFile analysis.txt'");
		
		
				
		pdbFile.close();
	}

}
