// a utility to find the pocket of a given ligand in a PDB structure

#include <BALL/common.h>
#include <BALL/FORMAT/PDBFile.h>
#include <BALL/KERNEL/system.h>

using namespace std;
using namespace BALL;

//calculate center of mass of a given residue using average
Vector3 calculate_center(Residue res) {
	//create center Vector3
	Vector3 center;
	//iterate over all atoms in the file
	for (AtomIterator ai = res.beginAtom(); +ai; ++ai) {
		//get position and sum up
		Vector3 pos = ai->getPosition();
		center = center + pos;
	}
	//average by number of atoms
	center = center / res.countAtoms();
	return center;
}

//calculate euler dist
float euler_dist(Vector3 from, Vector3 to) {
	return sqrt(pow((from[0]-to[0]),2)+pow((from[1]-to[1]),2)+pow((from[2]-to[2]),2));
}

//extract all residues in the vicinity of the given center
//takes the pdbfile, the center to calculate from and a distance
Protein extract_res(System pdbfile, Vector3 center, int dist) {
	Protein pocket;
	for (ResidueIterator rif = pdbfile.beginResidue(); +rif; ++rif) {
		//get center of current residue
		Vector3 res_center = calculate_center(*rif);
		//check if dist between given center and center
		//current res is below dist threshold not zero and
		// is not a HOH
		string resname = rif->getName();
		float calc_dist = euler_dist(res_center, center);
		if(calc_dist <= dist && calc_dist != 0 && resname != "HOH") {
			//found residue, store it
			Residue* res = new Residue(*rif);
			pocket.insert(*res);
		}
	}
	return pocket;
}

int main(int argc, char** argv)
{
	// check for right number of arguments
	if (argc != 4)
	{
		//if wrong, print usage
		Log.error() << "Usage:" << argv[0] << " <PDB infile> <PDB outfile> <Ligand identifier>" << endl;
		return 1;
	}
	
	//set distance to extract residues in vicinity [A] (count be parsed as argument)
	int dist = 17;
	
	//read in file
	Log.info() << "Loading " << argv[1] << "..." << endl;
	PDBFile infile(argv[1]);
	
	// check if file exists
	if (!infile)
	{
		//and print error if not
		Log.error() << "error opening " << argv[1] << " for input." << endl;
		return 2;
	}

	//create system for input file
	System pdbfile;
	infile >> pdbfile;
	infile.close();
	Log.info() << "Read in " << pdbfile.countAtoms() << " atoms." << endl;
	
	//create new system to save the extracted data to a file
	
	
	//Iterate over all residues in the file
	int counter = 0;
	for (ResidueIterator ri = pdbfile.beginResidue(); +ri; ++ri) {
		//get name of current residue
		string resname = ri->getName();
		if(resname == argv[3]) {
			System out_sys;
			counter++;
			//add residue
			//calculate center of mass
			Log.info() << "Calculating center..." << endl;
			Vector3 center = calculate_center(*ri);
			Log.info() << "Center: " << center << endl;
			//extract residues in vicinity
			Log.info() << "Extracting residues in the vicinity..." << endl;
			Protein pocket = extract_res(pdbfile, center, dist);
			Log.info() << "Extraction completed, found " << pocket.countAtoms() << " atoms" << endl;
			//TODO: what to do with multiple ligand residues?
			//TODO: what if there is a alternate location identifier supplied? currently it averages over them
			//append pocket and ligand to system
			Residue* ligand = new Residue(*ri);
			pocket.append(*ligand);
			out_sys.append(pocket);
			//output save system to new pdb file
			string filename = argv[2];
			//remove ending
			filename = string(filename, 0, filename.rfind("."));
			//add counter and reattach pdb ending
			filename += to_string(counter) + string(".pdb");
			PDBFile outfile(filename, std::ios::out);
			outfile << out_sys;
			Log.info() << "Written to file " << filename << endl;
		}
	}
	
	
	
	return 0;
}