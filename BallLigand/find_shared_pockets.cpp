// a utility to find shared ligand pockets in multiple PDB structures
// NOTE: this tool will ONLY work on unix/linux systems
// TODO: could be made cross-platform using boost

#include <BALL/common.h>
#include <BALL/FORMAT/PDBFile.h>
#include <BALL/KERNEL/system.h>
#include <BALL/STRUCTURE/structureMapper.h>
#include <BALL/STRUCTURE/atomBijection.h>
#include <iostream>
#include <fstream>
#include <map>
#include <dirent.h>
#include <stdio.h>

using namespace std;
using namespace BALL;

int main(int argc, char** argv)
{	
	// check for right number of arguments
	if (argc != 3)
	{
		//if wrong, print usage
		Log.error() << "Usage:" << argv[0] << " <listfile (pdbids)> <working directory>" << endl;
		return 1;
	}
	
	//create map to store pdbid and ligand
	vector<string> pdbentries;

	//read in the listfile with the ids and ligands
	ifstream file ( argv[1] );
	string pdbid;
	while ( file.good() )
	{
		//read in pbdids and ligands from file
		getline ( file, pdbid, '\n' );
		//store each pair in the map
		pdbentries.push_back(pdbid);
	}
	
	//create workingdir if not existing
	string command = "";
	command += string("mkdir -p ") + argv[2] + string(" > /dev/null 2> /dev/null; ");
	system(command.c_str());
	
	//create subdir for extraction
	command = "";
	command += string("mkdir -p ") + argv[2] + string("/extracted_pockets/") + string(" > /dev/null 2> /dev/null; ");
	system(command.c_str());
	//save foldername for later
	string extracted_dir = argv[2] + string("/extracted_pockets/");
	
	//get path of the executable to invoke pdbdownloader
	char buffer[MAXPATHLEN];
	realpath(argv[0], buffer);
	string fullpath = buffer;
	fullpath = string(fullpath, 0, fullpath.rfind("/"));
	
	//iterate over map
	for(auto iter=pdbentries.begin(); iter!=pdbentries.end(); ++iter ) {
		//first call pdbdownloader to get files to the working directory
		string command = "";
		command += string(fullpath);
		command += string("/PDBDownload -id ") + string(*iter) + string(" -o ");
		command += string(argv[2]) + string(*iter) + string(".pdb");
		//remove debug output
		command += string(" > /dev/null 2> /dev/null; ");
		system(command.c_str());
		
		//second extract the pockets
		command = "";
		command += string(fullpath);
		command += string("/pocket_extractor ") + string(argv[2]) + string(*iter) + string(".pdb ");
		command += string(argv[2]) + string("/extracted_pockets/") + string(*iter) + string("_extracted_pocket.pdb");
		//remove debug output
		command += string(" > /dev/null 2> /dev/null; ");
		system(command.c_str());
	}
	
	//get size of folder
	vector<string> filenames;
	int filecounter = 0;
	struct dirent *dp;
	DIR* dirp = opendir(extracted_dir.c_str());
	while (( dp = readdir(dirp)) != NULL) {
		if(string(dp->d_name) != ".." && string(dp->d_name) != ".") {
			filecounter++;
			filenames.push_back(dp->d_name);
		}
	}
	(void)closedir(dirp);
	cout << "Extracted " << filecounter << " pockets" << endl;
	
	//do RMSD calculation each vs. each
	//create result vector to sort by RMSD
	map<float, string> rmsd_result;
	//loop over each file and compare it to each other file, save the result in the map
	for(vector<string>::size_type i = 0; i != filenames.size(); i++) {
		for(vector<string>::size_type j = i+1; j != filenames.size(); j++) {
			//do NOT compare extracted pockets from the same protein
			string pdbid1 = string(filenames[i], 0, filenames[i].rfind("_"));
			string pdbid2 = string(filenames[j], 0, filenames[j].rfind("_"));
			if(pdbid1 != pdbid2) {
				//call RMSD tool
				string command = "";
				command += string(fullpath);
				command += string("/pocket_mapper ") + extracted_dir + string(filenames[i]);
				command += string(" ") + extracted_dir + string(filenames[j]);
				//cout << "Comparing: " << filenames[i] << " - " << filenames[j] << endl;
				//system(command.c_str());
				//return stdout as buffer
				FILE *lsofFile_p = popen(command.c_str(), "r");
				char buffer[1024];
				char *line_p = fgets(buffer, sizeof(buffer), lsofFile_p);
				pclose(lsofFile_p);
				//cout << strtof(line_p, NULL) << endl;
				
				//store result in map
				string comparison = filenames[i]+" - "+filenames[j];
				float rmsd = strtof(line_p, NULL);
				rmsd_result.insert(pair<float,string>(rmsd, comparison));
			}
		}
	}
	
	//print sorted result
	for(auto iterator = rmsd_result.begin(); iterator != rmsd_result.end(); iterator++) {
		cout << iterator->second << ": " << iterator->first << endl;
	}
	
} 
 
