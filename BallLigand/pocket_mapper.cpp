// a utility to analyze ligand pockets in a PDB structure

#include <BALL/common.h>
#include <BALL/FORMAT/PDBFile.h>
#include <BALL/KERNEL/system.h>
#include <BALL/STRUCTURE/structureMapper.h>
#include <BALL/STRUCTURE/atomBijection.h>

using namespace std;
using namespace BALL; 

int main(int argc, char** argv)
{
	// check for right number of arguments
	if (argc != 3)
	{
		//if wrong, print usage
		Log.error() << "Usage:" << argv[0] << " <PDB infile one> <PDB infile two>" << endl;
		return 1;
	}
	
	map<String, Position> type_map;
	type_map["ALA"] = 0;
	type_map["GLY"] = 0;
	type_map["VAL"] = 0;
	type_map["LEU"] = 0;
	type_map["ILE"] = 0;
	type_map["SER"] = 0;
	type_map["CYS"] = 0;
	type_map["THR"] = 0;
	type_map["MET"] = 0;
	type_map["PHE"] = 0;
	type_map["TYR"] = 0;
	type_map["TRP"] = 0;
	type_map["PRO"] = 0;
	type_map["HIS"] = 0;
	type_map["LYS"] = 0;
	type_map["ARG"] = 0;
	type_map["ASP"] = 0;
	type_map["GLU"] = 0;
	type_map["ASN"] = 0;
	type_map["GLN"] = 0;

	double upper =     8.0;
	double lower =     4.0;
	double tolerance = 0.6;

	Matrix4x4       T;
	StructureMapper mapper;
	Size            no_ca;
	double          rmsd;

	// read in two proteins, of which the RMSD should be determined
	PDBFile pdbfile1(argv[1]);
	PDBFile pdbfile2(argv[2]);

	Protein protein1;
	Protein protein2;

	pdbfile1 >> protein1;
	pdbfile2 >> protein2;
	
	pdbfile1.close();
	pdbfile2.close();

	// determine Transformation of the c-alpha atoms
	T = mapper.mapProteins(protein1, protein2, type_map, no_ca, rmsd, upper, lower, tolerance);
	
	mapper.setTransformation(T);
	protein1.apply(mapper);
	
	//save aligned structure for easier comparison later on in ballview
	PDBFile outfile(argv[1], std::ios::out);
	outfile << protein1;
	outfile.close();
	
	/*
	 * NOTE: This part calculates the RMSD WITHOUT aligning the two structures!
	float rmsd = 0.0;

	//only use C alpha atoms
	//NOTE: this will only work with the GIT version of BALL, NOT the stable version
	AtomBijection atom_bijection;
	atom_bijection.assignCAlphaAtoms(protein1, protein2);
	
	//use the StructureMapper to map both systems onto each other
	StructureMapper mapper(protein1, protein2);
	
	//determine the RMSD
	rmsd = mapper.calculateRMSD(atom_bijection);
	*/
	
	cout << rmsd << endl;

}