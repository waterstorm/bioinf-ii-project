// a utility to analyze ligand pockets in multiple PDB structures
// NOTE: this tool will ONLY work on unix/linux systems
// TODO: could be made cross-platform using boost

#include <BALL/common.h>
#include <BALL/FORMAT/PDBFile.h>
#include <BALL/KERNEL/system.h>
#include <BALL/STRUCTURE/structureMapper.h>
#include <BALL/STRUCTURE/atomBijection.h>
#include <iostream>
#include <fstream>
#include <map>
#include <dirent.h>
#include <stdio.h>

using namespace std;
using namespace BALL;

int main(int argc, char** argv)
{	
	// check for right number of arguments
	if (argc != 3)
	{
		//if wrong, print usage
		Log.error() << "Usage:" << argv[0] << " <listfile (id - ligand)> <working directory>" << endl;
		return 1;
	}
	
	//create map to store pdbid and ligand
	map<string, string> pdbentries;

	//read in the listfile with the ids and ligands
	ifstream file ( argv[1] );
	string pdbid, ligandid;
	while ( file.good() )
	{
		//read in pbdids and ligands from file
		getline ( file, pdbid, '\t' );
		getline ( file, ligandid, '\n' );
		//convert to upper case
		transform(ligandid.begin(), ligandid.end(), ligandid.begin(), ::toupper);
		//store each pair in the map
		pdbentries[pdbid] = ligandid;
	}
	
	//create workingdir if not existing
	string command = "";
	command += string("mkdir -p ") + argv[2] + string(" > /dev/null 2> /dev/null; ");
	system(command.c_str());
	
	//create subdir for extraction
	command = "";
	command += string("mkdir -p ") + argv[2] + string("/extracted_pockets/") + string(" > /dev/null 2> /dev/null; ");
	system(command.c_str());
	//save foldername for later
	string extracted_dir = argv[2] + string("/extracted_pockets/");
	
	//get path of the executable to invoke pdbdownloader
	char buffer[MAXPATHLEN];
	realpath(argv[0], buffer);
	string fullpath = buffer;
	fullpath = string(fullpath, 0, fullpath.rfind("/"));
	
	//iterate over map
	for( auto iter=pdbentries.begin(); iter!=pdbentries.end(); ++iter ) {
		//first call pdbdownloader to get files to the working directory
		string command = "";
		command += string(fullpath);
		command += string("/PDBDownload -id ") + string(iter->first) + string(" -o ");
		command += string(argv[2]) + string(iter->first) + string(".pdb");
		//remove debug output
		command += string(" > /dev/null 2> /dev/null; ");
		system(command.c_str());
		
		//second extract the pockets
		command = "";
		command += string(fullpath);
		command += string("/pocket_identifier ") + string(argv[2]) + string(iter->first) + string(".pdb ");
		command += string(argv[2]) + string("/extracted_pockets/") + string(iter->first) + string("_extracted_pocket.pdb ");
		command += string(iter->second);
		//remove debug output
		command += string(" > /dev/null 2> /dev/null; ");
		system(command.c_str());
	}
	
	//do RMSD calculation each vs. each and save it to a matrix
	//get size of folder
	vector<string> filenames;
	int filecounter = 0;
	struct dirent *dp;
	DIR* dirp = opendir(extracted_dir.c_str());
	while (( dp = readdir(dirp)) != NULL) {
		if(string(dp->d_name) != ".." && string(dp->d_name) != ".") {
			filecounter++;
			filenames.push_back(dp->d_name);
		}
	}
	(void)closedir(dirp);
	cout << "Extracted " << filecounter << " pockets" << endl;
	
	//create RMSD matrix
	vector<std::vector<int> > matrix(filecounter, std::vector<int>(filecounter, 0));
	
	//loop over each file and compare it to each other file, save the result in the matrix
	for(vector<string>::size_type i = 0; i != filenames.size(); i++) {
		for(vector<string>::size_type j = i+1; j != filenames.size(); j++) {
			//call RMSD tool
			string command = "";
			command += string(fullpath);
			command += string("/pocket_mapper ") + extracted_dir + string(filenames[i]);
			command += string(" ") + extracted_dir + string(filenames[j]);
			//return stdout as buffer
			FILE *lsofFile_p = popen(command.c_str(), "r");
			char buffer[1024];
			char *line_p = fgets(buffer, sizeof(buffer), lsofFile_p);
			pclose(lsofFile_p);
		}
	}
	
} 
