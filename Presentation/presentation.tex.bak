\documentclass{beamer}

\usepackage[english]{babel}
%\usepackage[latin1]{inputenc}
\usepackage{amsmath,amsfonts,amssymb}
%\usepackage[applemac]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{algorithm2e}
%\usepackage{tabularx}

\usetheme{Boadilla}
%\usepackage{enumerate}

\usecolortheme{beaver}
\usefonttheme{professionalfonts}
\useinnertheme{rounded}
\usepackage{subfigure}
\usepackage[font=scriptsize]{caption}
\captionsetup[figure]{labelformat=empty}
\usepackage{booktabs}
\useoutertheme{smoothbars}
\usepackage{listings}

\title[Binding Pocket Analysis]{
  Binding Pocket Analysis
}
\institute[Uni T\"ubingen]{University of T\"ubingen}
\author[Lin, Rauch]{
  Baiyu Lin and Jens Rauch
}
\date{June 23, 2014}

\begin{document}
%%%%%
\begin{frame}
\maketitle
\end{frame}
%%%%%
\begin{frame}{Outline}
\begin{itemize}
	\item Introduction
	\item Approach
	\item Discussion
	\item Outlook
\end{itemize}
\end{frame}
%%%%%
\begin{frame}{Introduction - Task}
\begin{columns}
	\column{.5\textwidth}	
\begin{itemize}
	\item Find ligands and pockets in given PDB files
	\item Find a shared pocket and their ligands across all given PDB files
	\item Analyze them
	\begin{itemize}
		\item Common properties
		\item Differences
	\end{itemize}
\end{itemize}
	\column{.5\textwidth}
		\begin{figure}
			\includegraphics[scale=0.23]{../Report/pictures/white/ses_2ovm.png}
			\end{figure}
	\end{columns}	
\end{frame}
%%%%%
\begin{frame}{Approach - Prerequisites}
\begin{itemize}
	\item BALL API
	\item C++ programming language
	\item Writing BALL utilities/plug-ins
	\item BALLView
\end{itemize}
\end{frame}
%%%%%
\begin{frame}{Approach - \texttt{pocket extractor}}
\begin{columns}
	\column{.5\textwidth}	
\begin{itemize}
	\item Find all ligands
	\item Extract their pockets
	\begin{itemize}
		\item Center of mass
		\item Residues in vicinity (17 \r{A})
		\item Euclidean distance
	\end{itemize}
\end{itemize}
	\column{.5\textwidth}
		\begin{figure}
			\includegraphics[scale=0.28]{../Report/pictures/white/complete_desc.png}
		\end{figure}
	\end{columns}	
\end{frame}
%%%%%
\begin{frame}{Approach - \texttt{pocket mapper}}
\begin{columns}
	\column{.5\textwidth}	
\begin{itemize}
	\item Map and align two pockets using BALL API
	\item Apply transformation to PDB files
	\item Return RMSD
\end{itemize}
	\column{.5\textwidth}
		\begin{figure}
			\includegraphics[scale=0.15]{../Report/pictures/white/unaligend.png}
		\end{figure}
		\begin{figure}
			\includegraphics[scale=0.15]{../Report/pictures/white/aligend.png}
		\end{figure}
	\end{columns}	
\end{frame}
%%%%%
\begin{frame}{Approach - \texttt{find shared pocket}}
\begin{itemize}
	\item Input PDB list
	\item Download given PDB files
	\item Call pocket extractor
	\item Call pocket mapper on all pairs
\end{itemize}
\end{frame}
%%%%%
\begin{frame}{Given Proteins}
Progesterone and estrogen receptors
\begin{columns}
	\column{.33\textwidth}
	\\2GPV\\
	\includegraphics[scale=0.15]{./pics/2gpv_bio_r_500.jpg}\\
	2JF9\\
		\includegraphics[scale=0.15]{./pics/2jf9_bio_r_500.jpg}
	\column{.33\textwidth}
	2JFA\\
		\includegraphics[scale=0.15]{./pics/2jfa_bio_r_500.jpg}\\
	2OVH\\	
		\includegraphics[scale=0.15]{./pics/2ovh_bio_r_500.jpg}
	\column{.33\textwidth}
	2OVM\\
		\includegraphics[scale=0.15]{./pics/2ovm_bio_r_500.jpg}\\
	3KMZ\\
		\includegraphics[scale=0.15]{./pics/3kmz_bio_r_500.jpg}
\end{columns}
\end{frame}
%%%%%
\begin{frame}{Results - Found ligands and pockets}
\begin{figure}[H]
\centering
\includegraphics[scale=0.23]{../Report/pictures/white/all-ligands.png}
\includegraphics[scale=0.32]{../Report/pictures/white/all-pockets-color.png}
\end{figure}
\end{frame}
%%%%%
\begin{frame}{Results - Found ligands in one pocket}
\begin{figure}[H]
\centering
\includegraphics[scale=0.24]{../Report/pictures/white/ses-all.png}
\includegraphics[scale=0.24]{./pics/lig_RAL_EQO_ASO_pocket_2GPV.png}
\end{figure}
\end{frame}
%%%%%
\begin{frame}{Discussion - Flexibility}
\begin{figure}[H]
\centering
\subfigure[2OVM ligand]{\includegraphics[width=0.35\textwidth]{../Report/pictures/white/2ovh_ligand2.png}}
\hspace{0.2\textwidth}
\subfigure[2JF9 ligand]{\includegraphics[width=0.35\textwidth]{../Report/pictures/white/2jf9_ligand.png}}
\end{figure}
\end{frame}
%%%%%
\begin{frame}{Discussion - H-bond}
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{../Report/pictures/white/all-ligands1.png}
\end{figure}
\end{frame}
%%%%%
\begin{frame}{Discussion - Shared nitrogen, H-bond}
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{../Report/pictures/white/h-bond-2_61_white.jpg}
\end{figure}
\end{frame}
%%%%%
\begin{frame}{Discussion - Shared oxygen, H-bond}
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{../Report/pictures/white/h-bond-2_74_white.jpg}
\end{figure}
\end{frame}
%%%%%
\begin{frame}{Discussion - Residues}
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{../Report/pictures/residue_sharedPockets.png}
\caption{\label{residue_percentage_sharedPockets}The percentage of every residue type in the 6 similar binding pockets}
\end{figure}
\end{frame}
%%%%%
\begin{frame}{Discussion - Residues}
\begin{figure}[H]
\centering
\includegraphics[scale=0.41]{../Report/pictures/2JF9_2JFA.png}
\caption{\label{2JF9_2JFA}2JF9 and 2JFA (estrogen receptors)}
\includegraphics[scale=0.42]{../Report/pictures/2OVH_2OVM.png}
\caption{\label{2OVH_2OVM}2OVH and 2OVM (progesterone receptors)}
\end{figure}
\end{frame}
%%%%%
\begin{frame}{Outlook}
\begin{columns}
	\column{.5\textwidth}	
\begin{itemize}
	\item Drug design approach
	\begin{itemize}
		\item 3KMZ ligand
		\item Missing h-bond
		\item Higher affinity 
	\end{itemize}
	\item Automated pocket finding
	\begin{itemize}
		\item Count $C_{\alpha}$
		\item Sort by RMSD
	\end{itemize}
\end{itemize}
	\column{.5\textwidth}
		\begin{figure}
			\includegraphics[scale=0.3]{../Report/pictures/white/no-nitrogen_annotated_white.png}
			\end{figure}
	\end{columns}	
\end{frame}
%%%%%
\begin{frame}
	\begin{center}
		\huge Thank you for your attention\\Questions?
	\end{center} 
\end{frame}
%%%%%
\end{document}}