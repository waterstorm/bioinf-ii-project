# Binding pocket analysis #

A project for Bioinformatics II.

### Note ###
This project will only work with the latest GIT version of BALL available at:
http://ball-trac.bioinf.uni-sb.de/wiki/DevelopmentEnvironment

## Install ##

* Put the files pocket_identifier.cpp and pocket_mapper.cpp in the folder ./BALL/source/APPLICATIONS/UTILITES
* Edit the file ./BALL/source/APPLICATIONS/UTILITES/BALLUtilities.cmake and add pocket_identifier and pocket_mapper to the EXECUTABLES_LIST
* recompile code by executing make (if you want to use multithreading use make -j numberofcores)