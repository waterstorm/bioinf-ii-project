\documentclass[10pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{float}
\restylefloat{table}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{setspace}
\usepackage{subfigure}
% Caption Settings:
\usepackage[hang]{caption}
\DeclareCaptionLabelFormat{bez}{#2}
\renewcommand{\thesection}{\arabic{section}}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{lastpage}

\title{Bioinformatics II - Project \\ Binding pocket analysis \\ SS 2014}
\date{\today}
\author{Baiyu Lin \and Jens Rauch}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\small{Bioinformatics II - Project}}
\fancyhead[R]{\small{Baiyu Lin and Jens Rauch}}
\fancyfoot[C]{Page \thepage\ of \pageref{LastPage}}

\begin{document}
\doublespacing
\maketitle
\singlespacing
\setcounter{page}{1}



\section{Abstract}
The increasing number of enzymes, especially proteolytic enzymes, are proved to play a role in some diseases (e.g. AIDS) and thus, are potential drug targets. Hence developing inhibitors, which can dock to these enzymes with a higher affinity and therefore inhibit them, becomes a urgent goal for drug design. However, the docking, especially protein-protein docking is, because of the flexibility of enzymes, still a big issue. Identification of binding pockets, i.e. active sites, play an important role for finding better inhibitors. In an effort to get useful conclusion about the properties of binding pockets for improved docking, to contribute to drug development, we focus on a set of experimentally resolved enzyme-ligand complexes deposited in the PDB. In order to do this, we are utilizing the BALL library and use BALLView to conclude information about geometrical complementarity and the residues physicochemical properties.

\section{Introduction}
Many proteases are proven to be related to some diseases, such as cancer, HIV infection (AIDS), diabetes, etc.\cite{bvgh}. Therefore, more and more proteolytic enzymes are regarded as therapeutic targets for the development of inhibitors. In other words, in order to find suitable ligands to develop drug candidates, we should study the factors, which are related to the enzymatic function of the proteins. In 1894, Hermann Emil Fischer published the “lock and key” model to describe enzyme substrate interactions. He postulated that the enzyme must be a geometric complement of a substrate like a key and a lock, in order to initiate a chemical action on each other\cite{Fischer}. It seems, that for developing an inhibitor, we need to find a ligand, which can be the geometric complement of the enzyme. However, enzymes has been demonstrated to be more flexible instead of rigid\cite{Erwin}. Although geometrical complementarity plays an important role in the recognition of two binding partners, it has been proven, that it is often far from perfect\cite{Liang}. That means, in addition to the geometrical complementarity, the molecule recognition between enzyme and substrate also requires physicochemical complementarity\cite{Tsai}.

Binding pockets are the key active sites in enzymes. Hence, detection, comparison and analyses of binding pockets are crucial for structure-based drug design. In this study, we focus on a set of experimentally resolved enzyme-ligand complexes deposited in the PDB, to analysis binding pockets and get conclusions about the geometrical complementarity related with pocket shape and the residues' physicochemical properties. We expect that these conclusions can assist binding site identification and contribute to the development of inhibitors for the known drug targets. At the same time, hopefully these conclusions about the properties can be useful for estimating drug-ability of other proteins. 

\section{Methods}

\subsection{RMSD}
The Root-Mean-Square Deviation (RMSD)\cite{RMSD}, is a measure for estimating the distance of two molecules by calculating the average distance between the atoms of superimposed molecules. It can be used for measuring the similarity of protein conformations in 3D structures, usually by calculating the RMSD of the $C_{\alpha}$ atom coordinates. The RMSD of N pairs of atoms in molecule a and b is defined as:\\\\
RMSD $=\sqrt{\dfrac{1}{N}\sum{(x_a-x_b)^2+(y_a-y_b)^2+(z_a-z_b)^2}}$\\ 

\subsection{MCSS}
Graph-based molecule comparison is a method for estimating the similarity between pairs of molecules. The basic idea is to find the Maximum Common Substructure (MCSS)\cite{MCSS} of two molecules. The bigger the MCSS the more similar the molecules. Therefore, the MCSS can be used for evaluating the similarity of ligands as well as their scaffolds. The "Moss MCSS Molecule Similarity"\cite{Moss} in KNIME\cite{KNIME} is a method to estimate the similarity of molecules based on the MCSS. However, note that its output is the computed distances between the molecules. The bigger the value, the bigger distance between the molecules. A value of zero means they are the same, while one means they are totally different. 

\section{Approach}

\subsection{Overview}
Both of the geometrical complementarity and physicochemical complementarity play a role in the recognition of binding pocket and ligand to make sure the specificity of enzyme. In order to analyze the role of these two factors in binding of ligand to enzyme, we extract all the pockets of the given PDB files based on the center of mass of the bound ligands. Then collecting one similar pocket per PDB file, which is shared across all those PDB files, according to RMSD of every pairwise pockets based on the alignment. All of these should be handled by an utility written by us. After that, using this set of collected similar pockets as the analytic object to analyze the binding pockets via a molecular modeling and visualisation application, consequently, getting the analysis about geometrical complementarity and physicochemical complementarity.
 	
\subsection{Prerequisites}
For this project we used the programming language C++ to fully utilize the functions and features from the Biochemical Algorithms Library (BALL) framework\cite{ball}. In contrary to the most scripting languages, C++ is a more low-level language and differs greatly depending on the used operating system (OS). In our case Linux was used to get the job done and therefore the program is optimized for Linux and will not work on other systems such as windows, without modifying the source code. However this could be achieved using cross-platform libraries such as boost\cite{BoostSite}.

\subsubsection{BALL}
BALL is a comprehensive rapid application development framework for structural bioinformatics. With the help of BALL one can develop sophisticated C++ programs for Computational Molecular Biology and Molecular Modeling in a relatively short time.

\subsubsection{BALLView}
BALLView\cite{ballview} is an extension of BALL. It is used for visual representation of all kinds of molecules. In addition it allows for fast in-program scripting in python, which makes specific tasks fast and easy. In our project BALLView was used to compare different ligands and pockets to each other visually.

\subsubsection{PDB Files}
Six files were supplied from the Protein Data Bank\cite{pdb} in the PDB format. All these proteins are sharing a similar ligand pocket, which should be found in this project. They had to be parsed and analyzed.

\subsection{Writing a BALL Utility}
All subsequent tasks were completed by writing one or multiple BALL utilities. To get started we downloaded the latest GIT release of BALL from the repository and compiled it. In addition, we created for each utility a new C file in the folder \texttt{source/APPLICATIONS/UTILITIES/} and added them to the cmake file in the same folder. After writing the code and recompiling, the written utilities were located in the bin folder.

\subsection{Extract Pockets}
The first task was solved by writing a utility called \texttt{pocket\_extractor}. Its task is to find all ligands in a given PDB file and extract the ligands with their pockets. The usage of this utility is:
\begin{verbatim}
pocket_extractor <PDB infile> <PDB outfile>
\end{verbatim}
While it enumerates the output file for each pocket it finds.\\\\
The ligands are supplied in the HETATM section of a PDB file. To be able to extract the pocket of a ligand we first needed to calculate the center of mass for the given ligand. Given the ligand residue the program should first calculate the center of mass which was done using the simple averaging approach:\\\\
Center of mass $=\frac{\sum{\texttt{atom coordinate}}}{\texttt{number of atoms}}$\\\\
With the help of this formula the center of mass can be calculated for any given residue.\\\\
The next step to extract the pocket of a ligand, was to extract all residues in the vicinity of the ligand. To extract the pocket a radius has to be set in which all residues are extracted and saved to another file. In order to calculate the distance of two residues in the 3D space, we used the center of the two residues and the euclidean distance measure:\\\\
$Dist = \sqrt{(x_1-x_2)^2+(y_1-y_2)^2+(z_1-z_2)^2}$\\\\
In this project a radius of 17 $\AA$ around the center of the ligand was used. All residues within this radius were extracted and saved to a new file for the pocket. The ligand was also added to this files, therefore each file contains a ligand and its corresponding pocket.

\subsection{Mapping and Distance}
The second task of the project was solved with a utility called \texttt{pocket\_mapper}, which is able to map two given proteins and calculate their distance using the RMSD. The usage of this utility is:
\begin{verbatim}
pocket_mapper <PDB infile one> <PDB infile two>
\end{verbatim}
First the transformation of the first to the second protein is calculated by mapping both proteins and in addition, the RMSD is returned. This transformation is then applied to the first protein to align both proteins to each other. To prevent realignments later on the aligned version of the proteins are saved after the transformation. This makes it possible to easily compare the saved PDB files for the pockets in BALLView.

\subsection{Finding all Pockets}
In order to easily extract and analyze all pockets for all given PDB files at once an additional utility has been created called \texttt{find\_shared\_pockets}. It will extract and align all the pockets in the given PDB files. The usage is:
\begin{verbatim}
find_shared_pockets <listfile (pdbids)> <working directory>
\end{verbatim}
Where the \texttt{list-file} is a simple text file containing all PDB IDs, one per line:
\begin{verbatim}
PDBID
PDBID
PDBID
\end{verbatim}
The utility will then download each file using the PDBDownload utility already implemented in BALL and call the \texttt{pocket\_extractor} on each downloaded PDB file. All the files will be placed in the supplied \texttt{working directory} for further analysis.\\
After extraction the utility analyzes the working directory and calls the \texttt{pocket\_mapper} each versus each extracted pocket, excluding the same PDB file. Pockets from the same PDB file are not compared, as the topic was to find a shared pocket across multiple PDB files.\\
The RMSD for each mapping will be returned (sorted) to the standard out of the terminal and can in Linux be saved to a file using the output redirection operator \texttt{>}.

\section{Results and Discussion}

\subsection{Finding the Pockets}
In order to analyze the given PDB structures, we created a \texttt{list-file} for our utility:
\begin{verbatim}
2OVH
2OVM
3KMZ
2GPV
2JF9
2JFA
\end{verbatim}
With the given files it extracted and aligned 40 pockets. These pockets were reviewed using BALLView and the shared pocket was used for each PDBID (see table\ref{tab:results}).
\begin{table}[H]
\centering%
\begin{tabular}{|c|c|c|}
\hline
\textbf{PDBID} & \textbf{Ligand} & \textbf{Pocket-name from utility}\\
\hline
2OVH & AS0 & 2OVH\_extracted\_pocket1\\
\hline
2OVM & AS0 & 2OVM\_extracted\_pocket1\\
\hline
3KMZ & EQO & 3KMZ\_extracted\_pocket3\\
\hline
2GPV & OHT & 2GPV\_extracted\_pocket1\\
\hline
2JF9 & OHT & 2JF9\_extracted\_pocket1\\
\hline
2JFA & RAL & 2JFA\_extracted\_pocket2\\
\hline
\end{tabular} 
\caption{Table of extracted pockets found to be shared across all the molecules. \texttt{PDBID} is the id from the PDB, \texttt{Ligand} is the name of the extracted ligand residue and \texttt{Pocket-name from utility} is the name of the extracted pocket from our utility from all the 40 extracted pockets (all files can be found in the supplementary)}
\label{tab:results}
\end{table}

\subsection{Analyzing the Pockets}
In order to get a first idea how the ligands fit into the pockets, figure~\ref{fig:seslig} shows the SES model representation of the pocket and the stick model of the ligand.

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{./pictures/white/ses_2ovm.png}
\caption{The SES model representation of the 2OVM pocket and the stick model of the 2OVM ligand}
\label{fig:seslig}
\end{figure}

In addition, to get an overview over all the aligned ligands and all the aligned pockets are shown in figure~\ref{fig:lig_pok}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.23]{./pictures/white/all-ligands.jpg}
\includegraphics[scale=0.32]{./pictures/white/all-pockets-color.jpg}
\caption{\texttt{a}: All the aligned ligands of the shared pockets are shown in direct comparison using the stick model. \texttt{b}: All the aligned shared pockets are shown in direct comparison using the stick model.}
\label{fig:lig_pok}
\end{figure}

Figure~\ref{fig:lig_pok} shows, that all of those 6 binding pockets look very similar to each other and just differ a bit in the overall shape and secondary structure. Although the 6 bound ligands differ a lot in the scaffold, the overall shape also looks very similar.

\subsubsection{Ligands}
As shown in figure \ref{fig:lig_pok}, the overall shape of the 6 bound ligands is similar, as they all bind to binding pockets with similar shapes. This states that the geometrical complementarity is related to the binding of binding pocket and ligand. What's more, from the results from table \ref{tab:results}, we know that, these 6 ligands are exactly 4 different compounds. Both ligands named ASO and both named OHT bind with their binding pocket with two conformations. 
That means the same ligand may, via its flexibility, bind to a bit different pockets. One of the most interesting things about the ligands is the fact that they differ a lot in the scaffold (which corresponds to the fixed part of a molecule without the functional groups), but they have a very similar overall shape nonetheless. Some of them are more rigid, as they have more ring systems which can not move as much as single bonds\ref{fig:rig}, while some of them are more flexible due to the single bonds\ref{fig:flex}. Therefore the scaffold corresponds to the flexibility, while the overall shape has to be similar to fit stereocally into the pocket.\\\\
Based on the MCSS molecule similarity, we estimate the distance of every pair of those 4 compounds utilizing KNIME. It shows, that almost all distances across these compounds, as well as their scaffolds, are bigger than 0.92. This makes clear that, these ligands are very different, although they have the similar overall shapes. Because of the geometrical complementarity, it is possible for these different ligands with similar shapes, to bind to the binding pockets with similar pocket shapes. One ligand can, due to the flexibility, bind with similar pockets. However, it can not bind with all of the pockets with similar shape. For example, ASO can bind with the pockets in 2OVH and 2OVM, in contrary to the other 4 pockets with a similar shape. It demonstrates geometrical complementarity is not enough to explain the specificity of enzymes. Physicochemical complementarity is also of great importance for the specificity of enzymes for recognizing the specific ligands.


\begin{figure}[H]
\centering
\subfigure[2OVM ligand \label{fig:rig}]{\includegraphics[width=0.25\textwidth]{./pictures/white/2ovh_ligand2.png}}
\hspace{0.2\textwidth}
\subfigure[2JF9 ligand \label{fig:flex}]{\includegraphics[width=0.25\textwidth]{./pictures/white/2jf9_ligand.png}}
\caption{The stick visualization of the 2OVM ligand compared to the same visualization for the 2JF9 ligand shows the different scaffold, while having a similar shape. The scaffold also corresponds to the flexibility.}
\label{fig:ligcomp}
\end{figure}

\subsubsection{H-Bonds}
An important physicochemical complemtentarity of the ligand-pocket interaction are hydrogen bonds. Looking again at figure~\ref{fig:lig_pok}(a) we can see that all of them, except one (see~\ref{subsubsec:diff}), share a nitrogen (right-top) and also share a oxygen (left-top). It can be assumed that it is no coincidence that they all share these positions. Also looking at the pockets of the given ligands we can identify the partner atoms for a hydrogen bond. We will show one or two representatives for each bond as it gets very confusing showing all pockets and ligands at once.\\\\
First we will look at the shared nitrogen (see figure~\ref{fig:nitro}). We can identify a possible oxygen partner atom for the hydrogen bond in its vicinity. Both atoms (N and O) are highlighted in yellow and the distance was calculated by BALLView to $2.617732 \AA$. As hydrogen bonds are known to be formed until a maximal distance of around $3.9 \AA$\cite{hbond}, it can be assumed, that a hydrogen bond is formed at this position.

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{./pictures/white/h-bond-2_61_white.png}
\caption{Looking at the shared nitrogen position of the ligands and their corresponding pocket, we can identify a hydrogen bond. The two partner atoms for the H-bond are highlighted in yellow, their distance is $2.618 \AA$. In this figure the two ligands RAL and OHT from 2JFA and 2JF9 are shown, including their pockets.}
\label{fig:nitro}
\end{figure}

The second position we will look at is the shared oxygen (see figure~\ref{fig:oxy}). Similar to the shared nitrogen, we found a partner atom for the hydrogen bond in the corresponding pockets. Again, they are highlighted in yellow and the distance was calculated to $2.740701 \AA$, which is still fine for a hydrogen bond.

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{./pictures/white/h-bond-2_74_white.png}
\caption{Looking at the shared oxygen position of the ligands and their corresponding pocket, we can identify a hydrogen bond. The two partner atoms for the H-bond are highlighted in yellow, their distance is $2.741 \AA$. In this figure the two ligands from 2OVM and 2OVH are shown, including their pockets.}
\label{fig:oxy}
\end{figure}

\subsubsection{Peculiar Differences}
\label{subsubsec:diff}
The ligands and pockets have a lot things in common, however, there are peculiar differences in some of the ligands. A good example for this is the 3KMZ ligand, which misses the nitrogen, all other ligands have (see figure~\ref{fig:nonitro}). The overall shape of 3KMZ is similar to the other ligands, as it needs to fit in the pocket. However the binding affinity to the pocket will be lower compared to the other ligands, as there is no hydrogen bond in this case.

\begin{figure}[H]
\centering
\subfigure[3KMZ ligand \label{fig:nonitrolig}]{\includegraphics[width=0.3\textwidth]{./pictures/white/3kmz_ligand.png}}
\hspace{0.1\textwidth}
\subfigure[The 3KMZ and the 2JF9 ligand with their pockets, focused on the (missing) nitrogen position \label{fig:nonitrocomp}]{\includegraphics[width=0.5\textwidth]{./pictures/white/no-nitrogen_annotated_white.png}}
\caption{The 3KMZ ligand, is a bit different than the other ligands. While the overall shape is the same, it misses the nitrogen, and therefore a hydrogen bond, resulting in a lower affinity to the pocket. The sub-figure~\ref{fig:nonitrocomp} compares 3KMZ with 2JF9 and shows the differences at the nitrogen site.}
\label{fig:nonitro}
\end{figure}

Another difference can be seen looking at the 3KMZ ligand at the shared oxygen of the ligands (see figure~\ref{fig:diffox}). It can easily be seen, that the shared oxygen part of the 3KMZ ligand is significantly longer than in the other ligands and therefore, to form a hydrogen bond, the pocket has also to look different. And again, this can be seen in figure~\ref{fig:diffox}, looking at the highlighted yellow atoms. However, the distance here was calculated to $3.220867 \AA$ and is therefore a bit longer than the other ones, but it is still in the radius to form a hydrogen bond.

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{./pictures/white/diff-hydro_white.png}
\caption{Differences at the shared oxygen site, in particular the 3KMZ ligand compared to the 2OVH ligand and their corresponding pocket. The distance between the two partner atoms for the possible hydrogen bond is around $3.221 \AA$.}
\label{fig:diffox}
\end{figure}

All these small differences in the 3KMZ ligand also lead to a bit different shape of its binding pocket. In figure~\ref{fig:3kmzses} shows the SES model of the 3KMZ pocket, with the ligand inside represented with the stick model.

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{./pictures/white/ses_3kmz.png}
\caption{The SES model representation of the 3KMZ pocket and the stick model of the 3KMZ ligand}
\label{fig:3kmzses}
\end{figure}

\subsubsection{Residues in binding pocket}
In order to get more information about the physicochemical properties in the binding pockets, we count the frequency of every residue type in the 6 similar pockets, in all of the extracted pockets and in all of the given PDB files (see figure \ref{residue_percentage} and \ref{residue_percentage_sharedPockets}).\\
It is obvious that, no matter in the whole sets or in the individual pockets, leusine still takes up the distinct biggest percentage. It can be assume that there are leucine-rich repeats(LRR) in the receptors related to the transcription, as all of these 6 given proteins are receptors in transcription.\\\\
The other most interesting aspect related to the residues, can be easy found in figure\ref{residue_percentage_sharedPockets}. The distribution of residue types in the 6 similar binding pockets can be divided into 4 groups. Although the overall shapes of these 6 pockets are similar, the frequency of residue types in them is obviously different. What's more, as mentioned above, the 6 sets of redidue frequency can be divided into 4 groups, as 2JF9 and 2JFA share almost the same distribution as well as 2OVH and 2OVM. It demonstrates that the residue types in binding pockets are related to the function of proteins, as both of 2JF9 and 2JFA are estrogen receptors and both of 2OVH and 2OVM are progesterone receptors, while 3KMZ and 2GPV are another two different recptors. According to figure \ref{residue_percentage_sharedPockets}, we can also assume that serine and glutamine are more important for the funcion of progesterone receptor. Glutamine acid and methionin play more important role in the function of estrogen receptor. In contract, threonine is obviously more significant for the function of the retinonic acid receptor, than for the other 3 receptors. 

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{./pictures/residue_percentage.png}
\caption{\label{residue_percentage}The percentage of every residue type in three different datasets. In blue: the frequency of residues in the 6 given PDB files. In violet: all of the 40 extracted pockets. In yellow: the data of the 6 shared pockets.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{./pictures/residue_sharedPockets.png}
\caption{\label{residue_percentage_sharedPockets}The percentage of every residue type in the 6 similar binding pockets}
\end{figure}

\section{Outlook}

\subsection{Drug Design}
One obvious approach to take this project further, would be the investigation of the missing nitrogen for the 3KMZ ligand. This could be used in drug design, to create a molecule which has a higher binding affinity to the given 3KMZ binding pocket and therefore would prevent the 3KMZ ligand from binding.

\subsection{Automated Pocket Finding}
Another interesting thing would be an improvement to the current implementation to find the shared pocket fully automated. It would probably not work in all cases. However in most cases it could be improved by not just calculating the RMSD, but also considering the number of $C^{\alpha}$ atoms. If it would just compare pockets having roughly the same size, the RMSD would be a much better value to really get the most similar pockets. This was obvious in our case, while 2OVM and 2OVH had just one pocket with nearly the same size they had low RMSD. In contrary, for the other files the RMSD was often lower comparing for example the 2OVM pocket with a much smaller pocket from an other file. In addition, it is possible to estimate the druggability pockets according to the frequency of redidue types by statical analyzing the known binding pockets.\\
So there probably would be a lot room for improvement to get it automatically done.

\bibliographystyle{unsrt}
\bibliography{report}




\end{document}




